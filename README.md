Tracie - Mesa Traces Continuous Integration System
==================================================

Home of the Mesa trace testing effort prototype.

## CI internals

### Docker image setup

The Mesa trace testing prototype is run inside a specially prepared docker
image. The image contains the latest dependencies needed to build Mesa and also
the tools for trace replay (currently apitrace and renderdoc).

Image setup uses helpers from the debian.yml template from
https://gitlab.freedesktop.org/wayland/ci-templates. This template is included
by our own yaml file.

The code to setup the image is based on the respective code from the Mesa
repository and resides in:
[.gitlab-ci/container/x86_build.sh](.gitlab-ci/container/x86_build.sh).

The images are cached based on the image tag, so any changes to to the install
script should be accompanied by a change to the `DEBIAN_TAG` variable in
[.gitlab-ci.yml](.gitlab-ci.yml)

### Building Mesa

By default we build and test Mesa master, but this can be overriden in manual
CI runs by setting the `MESA_COMMIT` and `MESA_REPO` variables.

### Traces definition file

The trace definition file contains information about the git repo/commit to get
the traces from, and a list of the traces to run along with their expected image
checksums on each device. An example:

```yaml
traces-db:
  repo: https://gitlab.freedesktop.org/gfx-ci/tracie/traces-db
  commit: master

traces:
  - path: glmark2/jellyfish.rdc
    expectations:
      - device: intel-0x3185
        checksum: 58359ea4caf6ad44c6b65526881bbd17
      - device: vmware-llvmpipe
        checksum: d82267c25a0decdad7b563c56bb81106
  - path: supertuxkart/supertuxkart-antediluvian-abyss.rdc
    expectations:
      - device: intel-0x3185
        checksum: ff827f7eb069afd87cc305a422cba939
```

The traces-db entry can be absent, in which case it is assumed that the
current directory is the traces-db directory.

Adding a new trace to the list involves commiting the trace to the git repo and
adding an entry to the `traces` list. The reference checksums can be calculated
with the [image_checksum.py](.gitlab-ci/tracie/image_checksum.py) script.

### Trace-db repos

The trace-db repos are assumed to be git repositories using LFS for their trace
files. This is so trace files can be checkout out and replayed individually,
thus reducing storage requirements during CI runs.

### Replaying traces

Mesa traces CI uses a set of scripts to replay traces and check the output
against reference checksums.

The high level script [tracie-runner.sh](.gitlab-ci/tracie-runner.sh) accepts
a traces definition file and the type of traces (apitrace/renderdoc) to run:

    tracie-runner.sh .gitlab-ci/traces.yml renderdoc

tracie-runner.sh copies produced artifacts to the `$CI_PROJECT_DIR/result`
directory. By default, created images from traces are only stored in case of a
checksum mismatch. The `TRACIE_STORE_IMAGES` CI/environment variable can be set
to `1` to force storing images, e.g., to get a complete set of reference
images.

At a lower level the
[dump_trace_images.py](.gitlab-ci/tracie/dump_trace_images.py) script is
called, which replays a trace, dumping a set of images in the process. By
default only the image corresponding to the last frame of the trace is dumped,
but this can be changed with the `--calls` parameter. The dumped images are
stored in a subdirectory `test/<device-name>` next to the trace file itself,
with names of the form `tracefilename-callnum.png`.  The full log of any
commands used while dumping the images is also saved in a file in the
'test/<device-name>' subdirectory, named after the trace name with '.log'
appended.

Examples:

    python3 dump_traces_images.py --device-name=vmware-llvmpipe mytrace.trace
    python3 dump_traces_images.py --device-name=vmware-llvmpipe --calls=2075,3300 mytrace.trace

### Running the replay scripts locally

It's often useful, especially during development, to be able to run the scripts
locally. The scripts require a recent version of apitrace being in the path,
and also the renderdoc python module being available.

For some guidance for building apitrace and renderdoc, check the
[debian-install.sh](.gitlab-ci/debian-install.sh) script.

To ensure python3 can find the renderdoc python module you need to set
`PYTHONPATH` to point to the location of `renderdoc.so` (binary python modules)
and `LD_LIBRARY_PATH` to point to the location of `librenderdoc.so`. In the
renderdoc build tree, both of these are in `renderdoc/<builddir>/lib`. Note
that renderdoc doesn't install the `renderdoc.so` python module.
